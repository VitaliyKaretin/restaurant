<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'restaurant');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|-6o,r|*M85=Zs{YGMyFbw?#c+vt6UGm];sp&$-8Q~/;UJKW$5r>MsNx}j:=Gat&');
define('SECURE_AUTH_KEY',  'gxE?gX?Jid<0E*}i{:(]g:C!7jCS-{ZBj2O9NfyNo``%-~Q10T-Njr8Aw1UAS?8k');
define('LOGGED_IN_KEY',    'tKMH]H^*~gV8mT,R?Fuu.] mv|9zw,~W?;v_t0-,/{W3WTu> @Rv8]B]{gT&D;zc');
define('NONCE_KEY',        'q72Gk$`;&eZCM7)mG16`v2p0n?bE[c<B03T=LU;~WpH.@R/KCvOOH}Tv-b~06|;m');
define('AUTH_SALT',        '%^cL{RAvUcMI-L*1en`n97v_1mfoAmI^|;O|btb6^K2likG[u@S~m9A|kXfuvtNK');
define('SECURE_AUTH_SALT', 'R&bwWZl`>._R:UZ2oX&>To[j8[lP/cvw{yBma_/D7Sz1M~l7VcLo5j8i)RM;0eoQ');
define('LOGGED_IN_SALT',   '<%s.#o32/lkCywE/11Z0SBoZ1Cu[::xohxhQgD80bdE^pYJss>2$UdfyaxO0l-XZ');
define('NONCE_SALT',       'l`Y#S_vgHW1-#x}9cJf.5unE9>R!A5Qg 8*5ae>5ZZ|Spd}i5I iBmc=ux&1fl[r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
