<?php

/*WP Enqueue Scripts */
function my_scripts_basic() 
{ 
 wp_register_script( 'my-owl-carousel', get_stylesheet_directory_uri() .'/libs/owl-carousel/owl.carousel.min.js', array('jquery'),'1.0', TRUE); 
 wp_register_script('scripts', get_stylesheet_directory_uri() .'/js/scripts.js', array(), '1.0', true );
 wp_register_script('fancybox', get_template_directory_uri() .'/libs/fancybox/jquery.fancybox.pack.js', array('jquery'), '1.0', true); 
 wp_register_script('nav', get_template_directory_uri() .'/libs/landing-nav/navigation.js', array('jquery'), '1.0', TRUE); 
 
 
 wp_enqueue_script( 'my-owl-carousel' );
 wp_enqueue_script( 'scripts' );
 wp_enqueue_script( 'fancybox' );
 wp_enqueue_script( 'nav' );
 } 
add_action( 'wp_enqueue_scripts', 'my_scripts_basic', 1 ); 

function asw_styles_basic() 
{ 
 /* —--------------------------------------------------------------------— */ 
 /* Register Stylesheets */ 
 /* —--------------------------------------------------------------------— */ 

wp_register_style( 'asw-basic', get_template_directory_uri() .'/style.css', array(), '1.0', 'all' ); 
wp_register_style( 'asw-media', get_template_directory_uri() .'/css/media.css', array(), '1.0', 'all' ); 
wp_register_style( 'asw-skeleton', get_template_directory_uri() . '/libs/bootstrap/bootstrap-grid-3.3.1.min.css', array(), '1', 'all' ); 
wp_register_style( 'asw-OwlCarousel', get_template_directory_uri() .'/libs/owl-carousel/owl.carousel.css', array(), '1.0', 'all' ); 
wp_register_style( 'FontAwesome',get_template_directory_uri() .'/libs/font-awesome-4.2.0/css/font-awesome.min.css', array(), '4.6.3', 'all' ); 
wp_register_style( 'fancybox',get_template_directory_uri() .'/libs/fancybox/jquery.fancybox.css' ,array(), '4.6.3', 'all' );
wp_enqueue_style( 'google-font-Yesterday','//fonts.googleapis.com/css?family=Yesteryear' ,array(), '', 'all' );//fonts.googleapis.com/css?family=Open+Sans
wp_enqueue_style( 'google-font-Yesterday','//fonts.googleapis.com/css?family=Open+Sans' ,array(), '', 'all' );

wp_enqueue_style( 'asw-OwlCarousel' );
wp_enqueue_style( 'asw-basic' ); 
wp_enqueue_style( 'asw-skeleton' ); 
wp_enqueue_style( 'FontAwesome' ); 
wp_enqueue_style( 'fancybox' ); 
wp_enqueue_style( 'asw-media' ); 
} 
add_action( 'wp_enqueue_scripts', 'asw_styles_basic', 1 );

/*my_menu */
register_nav_menu('main_navigation', 'Main Navigation');
register_nav_menu('main_navigation_fixed', 'Main Navigation Fixed');
register_nav_menu('main_navigation_sidebar', 'Main Navigation Sidebar');

/* Footer Area Widgets */
register_sidebar(array(
	'name' => __('Footer area widgets','asw-framework'),
	'id'   => 'footer-widgets',
	'description'   => __( 'These are widgets for footer area.','restourant' ),
	'before_widget' => '<div id="%1$s" class="col-md-4 widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
));

?>