<?php 

get_header(); ?>

<section class="home_page">
	<div id="toTop"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="logo">
					<a href="<?php echo home_url()?>">
						<h1>Restaurant</h1>
						<h2>SPECTRUM</h2>
					</a>
				</div>
				<div class="content">
					<h4>Best Website Design for Your Restaurant</h4>
					<h2>IF YOU THINK THAT YOU CAN'T CREATE A WEBSITE FOR YOUR OWN BUSINESS, YOU HAVEN'T USED MOTOCMS</h2>
					<h3>Build a website of your dreams, using a user-friendly website builder</h3>
					<button class="hvr-bounce-to-right">learn more</button>
				</div>		
			</div>
		</div>
	</div>
</section>
<section class="about_u">
	<div class="col-md-12">
			<div class="row">
				<div id="navigation">
					<div class="container_1_m2">
						<a href="<?php echo home_url()?>">
							<div class="logo2">
								<h3>Restaurant</h3>
								<h2>SPECTRUM</h2>
							</div>
						</a>	
						<button class="menu_button2 hidden-md hidden-lg"><i class="fa fa-bars"></i></button>
						<?php wp_nav_menu(array('theme_location' => 'main_navigation_fixed',
							'container' => true,
							'menu_id' => 'menu2',
							'menu_class' => 'clearfix',
							'fallback_cb' => false));
						?>
					</div>
				</div>
			</div>
	</div>

	<div class="container">
		<div class="content_2">
			<h1>About Us</h1>
			<h2>SPECTRUM IS THE <span class="colortext">BEST THEME</span> FOR YOUR RESTAURANT WEBSITE ON MOTOCMS</h2>
		</div>
		<div class="col-md-6">
		<div class="row">
				<div class="spectrum">
					<img src="<?php bloginfo('template_url');?>/images/Rectangle95.png" alt="">
					<h3>Spectrum Reastaurant is</h3>
					<h2>the best place for you to rest and catch a little break. We offer a wide range of dishes, 
					drinks and other services for the visitors of our restaurant. 
					For the regulars we have special discount program. Please, welcome to Spectrum Restaurant.</h2>
				</div>
				<div class="location">
					<img src="<?php bloginfo('template_url');?>/images/Rectangle42.png" alt="">
					<h3>Location</h3>
					<h2>4578 Marmora Road, Glasgow D04 89GR</h2>
				</div>
				<div class="work">
					<img src="<?php bloginfo('template_url');?>/images/Rectangle13.png" alt="">
					
					<h3>Working Hours</h3>
					<h2>10:00 am - 11:00 pm</h2>
				</div>
				<div class="call_us">
					<img src="<?php bloginfo('template_url');?>/images/Rectangle46.png" alt="">
					<h3>Call Us</h3>
					<h2>10:00 am - 11:00 pm</h2>
				</div>
		<button class="more">more about us</button>
		</div>
	</div>
		<div class="col-md-6">
			<div class="row">
				<img src="<?php bloginfo('template_url');?>/images/bg_2.png" class="img-responsive" alt="">
			</div>
		</div>
	</div>
		<div class="col-md-12">
			<div class="row">
				<div class="calltoaction">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<h2>choose comfortable for you Date and Time to get callBack</h2>
							</div>
							<div class="col-md-4">
							<button class="make_a_reservation">make a reservation</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>

<section class="our_team">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="team_hidden">
					<div class="team">
						<div class="row">
							<h1>Our team</h1>
							<?php
							$args = array( 'post_type' => 'our-team', 'posts_per_page' => 3 );
							$the_query = new WP_Query( $args );
							?>
							<?php $i = 1?>
							<?php if ( $the_query->have_posts() ) : ?>
							<?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
								<div class="col-md-4">
									<div class="row">
										<div class="chief_cook chief_sous">
											<?php echo get_the_post_thumbnail();?>
											<h2><?php the_title(); ?></h2>
											<h3><?php
												$custom = get_post_custom();
												echo $custom['wpcf-position'][0];
												?></h3>
											<h4><?php the_content(); ?></h4>
											<?php if($i == 2): ?>
											<?php echo "<button class=\"button_all_team\">all team</button>"?>
											<?php endif; ?>
											<?php $i++; ?>
											<?php wp_reset_postdata(); ?>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
							<?php endif; ?>

						</div>
					</div>
				</div>
				<div class="team_carousel hidden-md hidden-lg">
					<div class="team">
						<div class="row">
							<h1>Our team</h1>
								<div class="team_sider_container">
									<div class="next_button"><i class="fa fa-angle-right"></i></div>
									<div class="prev_button"><i class="fa fa-angle-left"></i></div>
									<div class="carousel">
										<?php
											$args = array( 'post_type' => 'our-team', 'posts_per_page' => 3 );
											$the_query = new WP_Query( $args );
										?>
										<?php if ( $the_query->have_posts() ) : ?>
										<?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
										<div class="chief_cook">
											<?php echo get_the_post_thumbnail();?>
											<h2><?php the_title(); ?></h2>
											<h3><?php
												$custom = get_post_custom();
												echo $custom['wpcf-position'][0];
												?></h3>
											<h4><?php the_content(); ?></h4>
											<?php wp_reset_postdata(); ?>
										</div>
										<?php endwhile; ?>
										<?php endif; ?>
									</div>
								</div>
							<button class="button_all_team">all team</button>
						</div>
					</div>			
				</div>
			</div>
		 </div>
	</div>
</section>

<section class="people_talk_about_us">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="content_people">
					<h1>People talk about us</h1>
					<div class="col-md-5">
						<div class="row">
							<div class="container-fluid">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4">
											<div class="people_group_2">
												<img src="<?php bloginfo('template_url');?>/images/people_1.png" alt="">
												<h2>JANE<br>KELEVAN</h2>
											</div>
										</div>
										<div class="col-md-4">
											<div class="people_group_2">
												<img src="<?php bloginfo('template_url');?>/images/people_2.png" alt="">
												<h2>PABLO EMILIO<br>ESCOBAR</h2>
											</div>
										</div>
										<div class="col-md-4">
											<div class="people_group_2">
												<img src="<?php bloginfo('template_url');?>/images/people_3.png" alt="">
												<h2>VINCENT<br>RAY</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="people_group_1">
											<img src="<?php bloginfo('template_url');?>/images/people_4.png" alt="">
											<h2>AARON<br>BLACK</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="row">
							<div class="container-fluid">
								<div class="col-md-12">
									<div class="row">
									<div class="col-md-4">
										<div class="people_group_1">
											<img src="<?php bloginfo('template_url');?>/images/people_5.png" alt="">
											<h2>BOB<br>MARLEY</h2>
										</div>
									</div>
									<div class="col-md-4">
										<div class="people_group_1">
											<img src="<?php bloginfo('template_url');?>/images/people_6.png" alt="">
											<h2>JOEY<br>SINCLAR</h2>
										</div>
									</div>
									<div class="col-md-4">
										<div class="people_group_1">
											<img src="<?php bloginfo('template_url');?>/images/people_7.png" alt="">
											<h2>SANDRA<br>BULLOCK</h2>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="container">
				<div class="row">
					<div class="sider_container">
						<div class="btn next next_button"><i class="fa fa-angle-right"></i></div>
						<div class="btn prev prev_button"><i class="fa fa-angle-left"></i></div>

						<div id="owl-carousel" class="owl-carousel owl-theme carousel">
							<?php
							$args = array( 'post_type' => 'people-talk-carousel', 'posts_per_page' => 5 );
							$the_query = new WP_Query( $args );

							?>
							<?php if ( $the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

									<div class="item">
									<p><?php the_content();?></p>
									<p><?php the_title();?></p>
									</div>
									<?php wp_reset_postdata(); ?>

								<?php endwhile;?>

							<?php endif; ?>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="gallery_content">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="gallery">
					<h1>gallery</h1>
					 <div class="container"> 
							<div class="row"> 
							<div class="gallery_container">
								<div class="row"> 
								<div class="col-md-12">
									<div class="col-md-4 thumb"> 
										<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo1.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo1.png" /> 
										</a> 
									</div> 
									<div class="col-md-4 thumb"> 
											<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo2.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo2.png" /> 
											</a> 
									</div> 
									<div class="col-md-4 thumb"> 
									   <a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo3.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo3.png" /> 
										</a> 
									 </div> 
								</div> 
								<div class="col-md-12">
									<div class="col-md-3 thumb"> 
										<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo4.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo4.png" /> 
										</a> 
									</div> 
									<div class="col-md-3 thumb"> 
											<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo5.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo5.png" /> 
											</a> 
									</div> 
									<div class="col-md-3 thumb"> 
									   <a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo6.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo6.png" /> 
										</a> 
									 </div> 
									<div class="col-md-3 thumb"> 
									   <a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo7.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo7.png" /> 
										</a> 
									</div> 
									</div> 
									<div class="col-md-12">
									<div class="col-md-2 thumb"> 
										<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo8.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo8.png" /> 
										</a> 
									</div> 
									<div class="col-md-2 thumb"> 
											<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo9.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo9.png" /> 
											</a> 
									</div> 
									<div class="col-md-2 thumb"> 
									   <a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo10.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo10.png" /> 
										</a> 
									 </div> 
									<div class="col-md-2 thumb"> 
									   <a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo11.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo11.png" /> 
										</a> 
									</div> 
									<div class="col-md-2 thumb"> 
										<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo12.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo12.png" /> 
										</a> 
									</div> 
									<div class="col-md-2 thumb"> 
											<a class="fancyimage" rel="group" href="<?php bloginfo('template_url');?>/images/gallery_photo13.jpg" alt=""> 
										  <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/gallery_photo13.png" /> 
											</a> 
									</div>
								</div>
								</div>
							</div> 
							<button class="all_gallery_button">all gallery</button>
						</div>
					</div>
				</div>
		</div>
	</div>
</section>
<section class="menu_content">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="menu_spectrum">
				<h1>menu</h1>
				<h2>SPECTRUM IS THE <span class="colortext"> BEST THEME</span> FOR YOUR RESTAURANT WEBSITE ON MOTOCMS</h2>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<?php
				$args = array( 'post_type' => 'menu', 'posts_per_page' => 6 );
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="foot">
							<div class="food_content">
								<div class="row">
									<?php echo get_the_post_thumbnail();?>
									<h2><?php the_title(); ?></h2>
									<h3><?php the_content(); ?></h3>
									<h4>$<?php echo get_post_field('price'); ?></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile;?>
				<?php endif; ?>
			</div>
		</div>
	<button class="download_button">download all menu. pdf</button>
	</div>
</section>

<section class="contact_us">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="contact">
				<img src="<?php bloginfo('template_url');?>/images/nuts.png" alt="">
					<h1>Contact us</h1>
					<?php echo do_shortcode('[contact-form-7 id="67" title="Contact Us"]')?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();?>
	
	


<script type="text/javascript"> 
    jQuery(document).ready(function($) { 
      $("a.fancyimage").fancybox(); 
    }); 
  </script> 
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>