<section class="footer">
	<div class="col-md-12">
		<div class="row">
			<div class="container">
				<div class="footer_container">
					<div class="row">
						<div class="col-md-12">
							<h1>Restaurant</h1>
							<h2>SPECTRUM</h2>
						</div>
					</div>
					<div class="row">
						<?php if ( dynamic_sidebar('Footer area widgets') ); ?>
					</div>
				</div>
			</div>
		</div>
	
	<div class="row">
		<div class="footer_black">
			<div class="container">
				<h2>Copyright © 2016</h2>
			</div>
		</div>
	</div>
</section>
<?php wp_footer();?>
</body>
</html>