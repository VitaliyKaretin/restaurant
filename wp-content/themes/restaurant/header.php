<!DOCTYPE html>
<html lang="ru">
<!--<![endif]-->
<head>
	<!--<meta charset="utf-8" />
	<title><?php bloginfo('name')?></title>
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="libs/bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" href="libs/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="libs/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="libs/bootstrap-3.3.2/css/bootstrap.min.css" rel="stylesheet"> 
	<link rel="stylesheet" href="libs/fancybox/jquery.fancybox.css" type="text/css" media="screen" /> 
	<link rel="stylesheet" href="css/media.css" />
	<link rel="stylesheet" href="css/normalize.css" />
	<link href='https://fonts.googleapis.com/css?family=Yesteryear' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
	
	<?php wp_head();?>
	</head>
	<body <?php body_class(); ?>>
	<header id="header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
				<button class="menu_button hidden-md hidden-lg"><i class="fa fa-bars"></i></button>	
					<ul id="menu" class="clearfix">
						<?php wp_nav_menu(array('theme_location' => 'main_navigation', 'container' => true, 'menu_id' => 'nav', 'items_wrap'=>'%3$s', 'fallback_cb' => false)); ?>
					</ul>
				</div>
			</div>
		</div>
	</header>	