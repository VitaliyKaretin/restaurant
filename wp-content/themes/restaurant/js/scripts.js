function home_parallax() {
    jQuery(window).scroll(function () {
        var coords, yPos = (-jQuery(window).scrollTop()/15);
        coords = yPos + 'px';
        jQuery('.home_page').css({
            'background-position': 'center '+ coords
        });

    });
}
jQuery(document).ready(function($) { 
      $("a.fancyimage").fancybox(); 
    }); 
	
// Выпадающее меню 
jQuery(document).ready(function($){
	home_parallax();
	  $('a').on('click', function(e){
		e.preventDefault();
	  });
	  $('#menu li,#menu2 li').hover(function () {
	   if ($(document).width() > 992) {
	   clearTimeout($.data(this,'timer'));
	   $('ul',this).stop(true,true).slideDown(200);
	   }
	  }, function () {
	   if ($(document).width() > 992) {
	  $.data(this,'timer', setTimeout($.proxy(function() {
		$('ul',this).stop(true,true).slideUp(200);
	  }, this), 100));
	   }
	  });
});
jQuery(document).ready(function($) {
//меню
$(".menu_button").click(function() {
	$("#menu li").slideToggle();
});
$(".menu_button2").click(function() {
	$("#menu2 li").slideToggle();
});

//фиксированное меню
 jQuery(function($) {
	        $(window).scroll(function(){
	            if($(this).scrollTop()>880){
	                $('#navigation').addClass('fixed');
	            }
	            else if ($(this).scrollTop()<880){
	                $('#navigation').removeClass('fixed');
	            }
	        });
	    });

 $(document).ready(function() {

 $("#owl-example").owlCarousel();

});

//карусель
var owl = $(".carousel");
	owl.owlCarousel({
		items : 1,
        itemsCustom : false,
        itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
		autoPlay : true,

	});
	
	$(".next_button").click(function(){
		owl.trigger("owl.next");
	});
	$(".prev_button").click(function(){
		owl.trigger("owl.prev");
	});
});

//Кнопка "Наверх"
	jQuery(function($) {
	$(window).scroll(function() {
	if($(this).scrollTop() != 0) {
	$('#toTop').fadeIn();
	} else {
	$('#toTop').fadeOut();
	}
	});
	$('#toTop').click(function() {
	$('body,html').animate({scrollTop:0},1000);
	});
	
});
